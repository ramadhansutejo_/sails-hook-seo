// Testing

module.exports = function(app) {
  return {
    routes: {
      before: {
        '/*': {
          skipAssets: true,
          fn: async function(req, res, next) {
            if (req.method === 'GET') {
              if (res.locals.title === undefined) {
                res.locals.title = 'Website Title'
              }
            }

            return next()
          }
        }
      }
    }
  }
}